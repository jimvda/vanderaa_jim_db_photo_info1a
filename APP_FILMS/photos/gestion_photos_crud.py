"""
    Fichier : gestion_photos_crud.py
    Auteur : OM 2021.03.16
    Gestions des "routes" FLASK et des données pour les photos.
"""
import sys

import pymysql
from flask import flash
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

from APP_FILMS import obj_mon_application
from APP_FILMS.database.connect_db_context_manager import MaBaseDeDonnee
from APP_FILMS.photos.gestion_photos_wtf_forms import FormWTFAjouterphotos
from APP_FILMS.photos.gestion_photos_wtf_forms import FormWTFDeletephotos
from APP_FILMS.photos.gestion_photos_wtf_forms import FormWTFUpdatephotos
from APP_FILMS.erreurs.exceptions import *
from APP_FILMS.erreurs.msg_erreurs import *

"""
    Auteur : OM 2021.03.16
    Définition d'une "route" /photos_afficher

    Test : ex : http://127.0.0.1:5005/photos_afficher

    Paramètres : order_by : ASC : Ascendant, DESC : Descendant
                id_photos_sel = 0 >> tous les photos.
                id_photos_sel = "n" affiche le photos dont l'id est "n"
"""


@obj_mon_application.route("/photos_afficher/<string:order_by>/<int:id_photos_sel>",
                           methods=['GET', 'POST'])
def photos_afficher(order_by, id_photos_sel):
    print("Emplacement afficher")
    if request.method == "GET":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion photos ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur Gestion photos {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                if order_by == "ASC" and id_photos_sel == 0:
                    strsql_photos_afficher = """SELECT id_photos, titre_photos, lieu_photos,
                     remarques_photos, chemin_photos, date_photos FROM t_photos WHERE 1"""
                    mc_afficher.execute(strsql_photos_afficher)
                elif order_by == "ASC":
                    # C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
                    # la commande MySql classique est "SELECT * FROM t_photos"
                    # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
                    # donc, je précise les champs à afficher
                    # Constitution d'un dictionnaire pour associer l'id du photos sélectionné avec un nom de variable
                    valeur_id_photos_selected_dictionnaire = {
                        "value_id_photos_selected": id_photos_sel}
                    strsql_photos_afficher = """SELECT id_photos, titre_photos, lieu_photos,
                     chemin_photos, date_photos, remarques_photos FROM t_photos WHERE id_photos = %(value_id_photos_selected)s"""

                    mc_afficher.execute(strsql_photos_afficher, valeur_id_photos_selected_dictionnaire)
                else:
                    strsql_photos_afficher = """SELECT id_photos, titre_photos, lieu_photos,
                    remarques_photos chemin_photos, date_photos, remarques_photos FROM t_photos ORDER BY id_photos DESC"""

                    mc_afficher.execute(strsql_photos_afficher)

                data_photos = mc_afficher.fetchall()

                print("data_photos ", data_photos, " Type : ", type(data_photos))

                # Différencier les messages si la table est vide.
                if not data_photos and id_photos_sel == 0:
                    flash("""La table "t_photos" est vide. !!""", "warning")
                elif not data_photos and id_photos_sel > 0:
                    # Si l'utilisateur change l'id_photos dans l'URL et que le photos n'existe pas,
                    flash(f"Le photos demandé n'existe pas !!", "warning")
                else:
                    # Dans tous les autres cas, c'est que la table "t_photos" est vide.
                    # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                    flash(f"Données photos affichés !!", "success")

        except Exception as erreur:
            print(f"RGG Erreur générale. photos_afficher")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)"
            # fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            flash(f"RGG Exception {erreur} photos_afficher", "danger")
            raise Exception(f"RGG Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Envoie la page "HTML" au serveur.
    return render_template("photos/photos_afficher.html", data=data_photos)


"""
    Auteur : OM 2021.03.22
    Définition d'une "route" /photos_ajouter

    Test : ex : http://127.0.0.1:5005/photos_ajouter

    Paramètres : sans

    But : Ajouter un photos pour un film

    Remarque :  Dans le champ "name_photos_html" du formulaire "photos/photos_ajouter.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/photos_ajouter", methods=['GET', 'POST'])
def photos_ajouter_wtf():
    form = FormWTFAjouterphotos()
    if request.method == "POST":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion photos ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur Gestion photos {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            if form.validate_on_submit():

                titre_photos_wtf = form.titre_photos_wtf.data
                titre_photos = titre_photos_wtf.capitalize()

                lieu_photos_wtf = form.lieu_photos_wtf.data
                lieu_photos = lieu_photos_wtf.capitalize()

                remarques_photos_wtf = form.remarques_photos_wtf.data
                remarques_photos = remarques_photos_wtf.capitalize()

                date_photos_wtf = form.date_photos_wtf.data
                date_photos = date_photos_wtf.capitalize()





                valeurs_insertion_dictionnaire = {"value_titre_photos": titre_photos,
                                                  "value_lieu_photos": lieu_photos,
                                                  "value_remarques_photos": remarques_photos,
                                                  "value_date_photos": date_photos
                                                  }

                print("valeurs_insertion_dictionnaire ", valeurs_insertion_dictionnaire)

                strsql_insert_photos = """INSERT INTO t_photos (id_photos,titre_photos,lieu_photos,remarques_photos,date_photos) VALUES (NULL,%(value_titre_photos)s,%(value_lieu_photos)s,%(value_remarques_photos)s,%(value_date_photos)s)"""
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(strsql_insert_photos, valeurs_insertion_dictionnaire)

                flash(f"Données insérées !!", "success")
                print(f"Données insérées !!")

                # Pour afficher et constater l'insertion de la valeur, on affiche en ordre inverse. (DESC)
                return redirect(url_for('photos_afficher', order_by='DESC', id_photos_sel=0))

        # ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur_photos_doublon:
            # Dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs/exceptions.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            code, msg = erreur_photos_doublon.args

            flash(f"{error_codes.get(code, msg)} ", "warning")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur_gest_empl_crud:
            code, msg = erreur_gest_empl_crud.args

            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Erreur dans Gestion photos CRUD : {sys.exc_info()[0]} "
                  f"{erreur_gest_empl_crud.args[0]} , "
                  f"{erreur_gest_empl_crud}", "danger")

    return render_template("photos/photos_ajouter_wtf.html", form=form)


"""
    Auteur : OM 2021.03.29
    Définition d'une "route" /photos_update

    Test : ex cliquer sur le menu "photos" puis cliquer sur le bouton "EDIT" d'un "photos"

    Paramètres : sans

    But : Editer(update) un photos qui a été sélectionné dans le formulaire "photos_afficher.html"

    Remarque :  Dans le champ "titre_photos_update_wtf" du formulaire "photos/photos_update_wtf.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/photos_update", methods=['GET', 'POST'])
def photos_update_wtf():
    # L'utilisateur vient de cliquer sur le bouton "EDIT". Récupère la valeur de "id_photos"
    id_photos_update = request.values['id_photos_btn_edit_html']

    # Objet formulaire pour l'UPDATE
    form_update = FormWTFUpdatephotos()
    try:
        print(" on submit ", form_update.validate_on_submit())
        if form_update.validate_on_submit():
            # Récupèrer la valeur du champ depuis "photos_update_wtf.html" après avoir cliqué sur "SUBMIT".
            # Puis la convertir en lettres minuscules.
            titre_photos_update = form_update.titre_photos_update_wtf.data
            titre_photos_update = titre_photos_update

            valeur_update_dictionnaire = {"value_id_photos": id_photos_update,
                                          "value_titre_photos": titre_photos_update}
            print("valeur_update_dictionnaire ", valeur_update_dictionnaire)

            str_sql_update_titre_photos = """UPDATE t_photos SET titre_photos = %(value_titre_photos)s WHERE id_photos = %(value_id_photos)s"""
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(str_sql_update_titre_photos, valeur_update_dictionnaire)

            flash(f"Donnée mise à jour !!", "success")
            print(f"Donnée mise à jour !!")

            # afficher et constater que la donnée est mise à jour.
            # Affiche seulement la valeur modifiée, "ASC" et l'"id_photos_update"
            return redirect(
                url_for('photos_afficher', order_by="ASC", id_photos_sel=id_photos_update))
        elif request.method == "GET":
            # Opération sur la BD pour récupérer "id_photos" et "noms" de la "t_photos"
            str_sql_id_photos = "SELECT id_photos, titre_photos FROM t_photos WHERE id_photos = %(value_id_photos)s"
            valeur_select_dictionnaire = {"value_id_photos": id_photos_update}
            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()
            mybd_curseur.execute(str_sql_id_photos, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()", vu qu'il n'y a qu'un seul champ "nom photos" pour l'UPDATE
            data_nom_emplacement = mybd_curseur.fetchone()
            print("data_nom_emplacement ", data_nom_emplacement, " type ", type(data_nom_emplacement), " photos ",
                  data_nom_emplacement["titre_photos"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "photos_update_wtf.html"
            form_update.titre_photos_update_wtf.data = data_nom_emplacement["titre_photos"]

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans photos_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans photos_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_matos_crud:
        code, msg = erreur_gest_matos_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_matos_crud} ", "danger")
        flash(f"Erreur dans photos_update_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_matos_crud.args[0]} , "
              f"{erreur_gest_matos_crud}", "danger")
        flash(f"__KeyError dans photos_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("photos/photos_update_wtf.html", form_update=form_update)


"""
    Auteur : OM 2021.04.08
    Définition d'une "route" /photos_delete

    Test : ex. cliquer sur le menu "photos" puis cliquer sur le bouton "DELETE" d'un "photos"

    Paramètres : sans

    But : Effacer(delete) un photos qui a été sélectionné dans le formulaire "photos_afficher.html"

    Remarque :  Dans le champ "titre_photos_delete_wtf" du formulaire "photos/photos_delete_wtf.html",
                le contrôle de la saisie est désactivée. On doit simplement cliquer sur "DELETE"
"""


@obj_mon_application.route("/photos_delete", methods=['GET', 'POST'])
def photos_delete_wtf():
    data_photos_attribue_photos_delete = None
    btn_submit_del = None
    # L'utilisateur vient de cliquer sur le bouton "DELETE". Récupère la valeur de "id_photos"
    id_photos_delete = request.values['id_photos_btn_delete_html']

    # Objet formulaire pour effacer le photos sélectionné.
    form_delete = FormWTFDeletephotos()
    try:
        print(" on submit ", form_delete.validate_on_submit())
        if request.method == "POST" and form_delete.validate_on_submit():

            if form_delete.submit_btn_annuler.data:
                return redirect(url_for("photos_afficher", order_by="ASC", id_photos_sel=0))

            if form_delete.submit_btn_conf_del.data:
                # Récupère les données afin d'afficher à nouveau
                # le formulaire "materiel/materiel_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
                data_materiel_attribue_photos_delete = session['data_materiel_attribue_photos_delete']
                print("data_materiel_attribue_photos_delete ", data_materiel_attribue_photos_delete)

                flash(f"Etes-vous sur de vouloir l'effacer de la DB ? C'est définitif.", "danger")
                # L'utilisateur vient de cliquer sur le bouton de confirmation pour effacer...
                # On affiche le bouton "Effacer materiel" qui va irrémédiablement EFFACER le materiel
                btn_submit_del = True

            if form_delete.submit_btn_del.data:
                valeur_delete_dictionnaire = {"value_id_photos": id_photos_delete}
                print("valeur_delete_dictionnaire ", valeur_delete_dictionnaire)

                str_sql_delete_materiel_photos = """DELETE FROM t_avoir_materiel WHERE fk_photos = %(value_id_photos)s"""
                str_sql_delete_id_photos = """DELETE FROM t_photos WHERE id_photos = %(value_id_photos)s"""
                # Manière brutale d'effacer d'abord la "fk_materiel", même si elle n'existe pas dans la "t_materiel_film"
                # Ensuite on peut effacer le materiel vu qu'il n'est plus "lié" (INNODB) dans la "t_materiel_film"
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(str_sql_delete_materiel_photos, valeur_delete_dictionnaire)
                    mconn_bd.mabd_execute(str_sql_delete_id_photos, valeur_delete_dictionnaire)

                flash(f"materiel définitivement effacé !!", "success")
                print(f"materiel définitivement effacé !!")

                # afficher les données
                return redirect(url_for('photos_afficher', order_by="ASC", id_photos_sel=0))

        if request.method == "GET":
            valeur_select_dictionnaire = {"value_id_photos": id_photos_delete}
            print(id_photos_delete, type(id_photos_delete))

            # Requête qui affiche tous les films qui ont le materiel que l'utilisateur veut effacer
            str_sql_materiel_photos_delete = """SELECT id_avoir_materiel, type_materiel, id_photos, titre_photos FROM t_avoir_materiel
                                            INNER JOIN t_materiel ON t_avoir_materiel.fk_materiel = t_materiel.id_materiel
                                            INNER JOIN t_photos ON t_avoir_materiel.fk_photos = t_photos.id_photos
                                            WHERE fk_photos = %(value_id_photos)s"""

            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()

            mybd_curseur.execute(str_sql_materiel_photos_delete, valeur_select_dictionnaire)
            data_materiel_attribue_photos_delete = mybd_curseur.fetchall()
            print("data_materiel_attribue_photos_delete...", data_materiel_attribue_photos_delete)

            # Nécessaire pour mémoriser les données afin d'afficher à nouveau
            # le formulaire "materiel/materiel_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
            session['data_materiel_attribue_photos_delete'] = data_materiel_attribue_photos_delete

            # Opération sur la BD pour récupérer "id_materiel" et "type_materiel" de la "t_materiel"
            str_sql_id_photos = "SELECT id_photos, titre_photos FROM t_photos WHERE id_photos = %(value_id_photos)s"

            mybd_curseur.execute(str_sql_id_photos, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()",
            # vu qu'il n'y a qu'un seul champ "nom materiel" pour l'action DELETE
            data_titre_photos = mybd_curseur.fetchone()
            print("data_titre_photos ", data_titre_photos, " type ", type(data_titre_photos),
                  " photos ",
                  data_titre_photos["titre_photos"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "materiel_delete_wtf.html"
            form_delete.titre_photos_delete_wtf.data = data_titre_photos["titre_photos"]

            # Le bouton pour l'action "DELETE" dans le form. "materiel_delete_wtf.html" est caché.
            btn_submit_del = False

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans photos_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans photos_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_matos_crud:
        code, msg = erreur_gest_matos_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_matos_crud} ", "danger")

        flash(f"Erreur dans photos_delete_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_matos_crud.args[0]} , "
              f"{erreur_gest_matos_crud}", "danger")

        flash(f"__KeyError dans photos_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("photos/photos_delete_wtf.html",
                           form_delete=form_delete,
                           btn_submit_del=btn_submit_del,
                           data_photos_associes=data_materiel_attribue_photos_delete)
