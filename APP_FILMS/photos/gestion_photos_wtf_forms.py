"""
    Fichier : gestion_photos_wtf_forms.py
    Auteur : OM 2021.03.22
    Gestion des formulaires avec WTF
"""
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms import SubmitField
from wtforms.validators import Length
from wtforms.validators import Regexp


class FormWTFAjouterphotos(FlaskForm):
    """
        Dans le formulaire "photos_ajouter_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    titre_photos_regexp = ""
    titre_photos_wtf = StringField("Titre", validators=[Length(min=1, max=50, message="min 2 max 50"),
                                                                       Regexp(titre_photos_regexp,
                                                                              message="Pas de chiffres, de caractères "
                                                                                      "spéciaux, "
                                                                                      "d'espace à double, de double "
                                                                                      "apostrophe, de double trait union")
                                                                       ])
    lieu_photos_regexp = ""
    lieu_photos_wtf = StringField("Lieu", validators=[Length(min=1, max=50, message="min 2 max 20"),
                                                                   Regexp(lieu_photos_regexp)

                                                                    ])
    remarques_photos_regexp = ""
    remarques_photos_wtf = StringField("Remarques", validators=[Length(min=1, max=50, message="min 2 max 20"),
                                                             Regexp(remarques_photos_regexp)

                                                             ])

    chemin_photos_wtf = StringField("Photo")

    date_photos_regexp = ""
    date_photos_wtf = StringField("Date de Capture",
                                         validators=[Length(min=2, max=50, message="min 2 max 20"),
                                                     Regexp(date_photos_regexp)

                                                                    ])
    submit = SubmitField("Ajouter une Photo")


class FormWTFUpdatephotos(FlaskForm):
    """
        Dans le formulaire "photos_update_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    titre_photos_update_regexp = ""
    titre_photos_update_wtf = StringField("Inscrire le nouveau titre. ",
                                              validators=[Length(min=2, max=100, message="min 2 max 20"),
                                                          Regexp(titre_photos_update_regexp,
                                                                 message="Pas de chiffres, de "
                                                                         "caractères "
                                                                         "spéciaux, "
                                                                         "d'espace à double, de double "
                                                                         "apostrophe, de double trait "
                                                                         "union")
                                                          ])
    submit = SubmitField("Update photos")


class FormWTFDeletephotos(FlaskForm):
    """
        Dans le formulaire "photos_delete_wtf.html"

        titre_photos_delete_wtf : Champ qui reçoit la valeur du photos, lecture seule. (readonly=true)
        submit_btn_del : Bouton d'effacement "DEFINITIF".
        submit_btn_conf_del : Bouton de confirmation pour effacer un "photos".
        submit_btn_annuler : Bouton qui permet d'afficher la table "t_photos".
    """
    titre_photos_delete_wtf = StringField("Effacer ce photos")
    submit_btn_del = SubmitField("Effacer photos")
    submit_btn_conf_del = SubmitField("Etes-vous sur d'effacer ?")
    submit_btn_annuler = SubmitField("Annuler")
