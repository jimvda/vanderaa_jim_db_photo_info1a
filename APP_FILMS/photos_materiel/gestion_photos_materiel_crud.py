"""
    Fichier : gestion_photos_materiel_crud.py
    Auteur : OM 2021.05.01
    Gestions des "routes" FLASK et des données pour l'association entre les photos et les materiel.
"""
import sys

import pymysql
from flask import flash
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

from APP_FILMS import obj_mon_application
from APP_FILMS.database.connect_db_context_manager import MaBaseDeDonnee
from APP_FILMS.photos_materiel.gestion_photos_materiel_wtf_forms import FormWTFAjouterphotosMateriel
from APP_FILMS.photos_materiel.gestion_photos_materiel_wtf_forms import FormWTFDeletephotosMateriel
from APP_FILMS.photos_materiel.gestion_photos_materiel_wtf_forms import FormWTFUpdatephotosMateriel
from APP_FILMS.erreurs.exceptions import *
from APP_FILMS.erreurs.msg_erreurs import *

"""
    Nom : photos_materiel_afficher
    Auteur : OM 2021.05.01
    Définition d'une "route" /photos_materiel_afficher

    But : Afficher les photos avec les materiel associés pour chaque photos.

    Paramètres : id_materiel_sel = 0 >> tous les photos.
                 id_materiel_sel = "n" affiche le photos dont l'id est "n"

"""


@obj_mon_application.route("/photos_materiel_afficher/<int:id_photos_sel>", methods=['GET', 'POST'])
def photos_materiel_afficher(id_photos_sel):
    if request.method == "GET":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as Exception_init_photos_materiel_afficher:
                code, msg = Exception_init_photos_materiel_afficher.args
                flash(f"{error_codes.get(code, msg)} ", "danger")
                flash(f"Exception _init_photos_materiel_afficher problème de connexion BD : {sys.exc_info()[0]} "
                      f"{Exception_init_photos_materiel_afficher.args[0]} , "
                      f"{Exception_init_photos_materiel_afficher}", "danger")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                strsql_materiel_photos_afficher_data = """SELECT id_photos, titre_photos, lieu_photos, remarques_photos, chemin_photos, date_photos,
                                                            GROUP_CONCAT(type_materiel) as Materielphotos FROM t_avoir_materiel
                                                            RIGHT JOIN t_photos ON t_photos.id_photos = t_avoir_materiel.fk_photos
                                                            LEFT JOIN t_materiel ON t_materiel.id_materiel = t_avoir_materiel.fk_materiel
                                                            GROUP BY id_photos"""
                if id_photos_sel == 0:
                    # le paramètre 0 permet d'afficher tous les photos
                    # Sinon le paramètre représente la valeur de l'id du photos
                    mc_afficher.execute(strsql_materiel_photos_afficher_data)
                else:
                    # Constitution d'un dictionnaire pour associer l'id du photos sélectionné avec un nom de variable
                    valeur_id_photos_selected_dictionnaire = {
                        "value_id_photos_selected": id_photos_sel}
                    # En MySql l'instruction HAVING fonctionne comme un WHERE... mais doit être associée à un GROUP BY
                    # L'opérateur += permet de concaténer une nouvelle valeur à la valeur de gauche préalablement définie.
                    strsql_materiel_photos_afficher_data += """ HAVING id_photos= %(value_id_photos_selected)s"""

                    mc_afficher.execute(strsql_materiel_photos_afficher_data,
                                        valeur_id_photos_selected_dictionnaire)

                # Récupère les données de la requête.
                data_materiel_photos_afficher = mc_afficher.fetchall()
                print("data_materiel ", data_materiel_photos_afficher, " Type : ",
                      type(data_materiel_photos_afficher))

                # Différencier les messages.
                if not data_materiel_photos_afficher and id_photos_sel == 0:
                    flash("""La table "t_photos" est vide. !""", "warning")
                elif not data_materiel_photos_afficher and id_photos_sel > 0:
                    # Si l'utilisateur change l'id_photos dans l'URL et qu'il ne correspond à aucun photos
                    flash(f"Le photos {id_photos_sel} demandé n'existe pas !!", "warning")
                else:
                    flash(f"Données photos et materiel affichés !!", "success")

        except Exception as Exception_photos_materiel_afficher:
            code, msg = Exception_photos_materiel_afficher.args
            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Exception photos_materiel_afficher : {sys.exc_info()[0]} "
                  f"{Exception_photos_materiel_afficher.args[0]} , "
                  f"{Exception_photos_materiel_afficher}", "danger")

    # Envoie la page "HTML" au serveur.
    return render_template("photos_materiel/photos_materiel_afficher.html",
                           data=data_materiel_photos_afficher)


"""
    nom: edit_materiel_photos_selected
    On obtient un objet "objet_dumpbd"

    Récupère la liste de tous les materiel du photos sélectionné par le bouton "MODIFIER" de "photos_materiel_afficher.html"

    Dans une liste déroulante particulière (tags-selector-tagselect), on voit :
    1) Tous les materiel contenus dans la "t_materiel".
    2) Les materiel attribués au photos selectionné.
    3) Les materiel non-attribués au photos sélectionné.

    On signale les erreurs importantes

"""


@obj_mon_application.route("/edit_materiel_photos_selected", methods=['GET', 'POST'])
def edit_materiel_photos_selected():
    if request.method == "GET":
        try:
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                strsql_materiel_afficher = """SELECT id_materiel, type_materiel FROM t_materiel ORDER BY id_materiel ASC"""
                mc_afficher.execute(strsql_materiel_afficher)
            data_materiel_all = mc_afficher.fetchall()
            print("dans edit_materiel_photos_selected ---> data_materiel_all", data_materiel_all)

            # Récupère la valeur de "id_photos" du formulaire html "photos_materiel_afficher.html"
            # l'utilisateur clique sur le bouton "Modifier" et on récupère la valeur de "id_photos"
            # grâce à la variable "id_photos_materiel_edit_html" dans le fichier "photos_materiel_afficher.html"
            # href="{{ url_for('edit_materiel_photos_selected', id_photos_materiel_edit_html=row.id_photos) }}"
            id_photos_materiel_edit = request.values['id_photos_materiel_edit_html']

            # Mémorise l'id du photos dans une variable de session
            # (ici la sécurité de l'application n'est pas engagée)
            # il faut éviter de stocker des données sensibles dans des variables de sessions.
            session['session_id_photos_materiel_edit'] = id_photos_materiel_edit

            # Constitution d'un dictionnaire pour associer l'id du photos sélectionné avec un nom de variable
            valeur_id_photos_selected_dictionnaire = {
                "value_id_photos_selected": id_photos_materiel_edit}

            # Récupère les données grâce à 3 requêtes MySql définie dans la fonction materiel_photos_afficher_data
            # 1) Sélection du photos choisi
            # 2) Sélection des materiel "déjà" attribués pour le photos.
            # 3) Sélection des materiel "pas encore" attribués pour le photos choisi.
            # ATTENTION à l'ordre d'assignation des variables retournées par la fonction "materiel_photos_afficher_data"
            data_materiel_photos_selected, data_materiel_photos_non_attribues, data_materiel_photos_attribues = \
                materiel_photos_afficher_data(valeur_id_photos_selected_dictionnaire)

            print(data_materiel_photos_selected)
            lst_data_photos_selected = [item['id_photos'] for item in data_materiel_photos_selected]
            print("lst_data_photos_selected  ", lst_data_photos_selected,
                  type(lst_data_photos_selected))

            # Dans le composant "tags-selector-tagselect" on doit connaître
            # les materiel qui ne sont pas encore sélectionnés.
            lst_data_materiel_photos_non_attribues = [item['id_materiel'] for item in
                                                            data_materiel_photos_non_attribues]
            session[
                'session_lst_data_materiel_photos_non_attribues'] = lst_data_materiel_photos_non_attribues
            print("lst_data_materiel_photos_non_attribues  ", lst_data_materiel_photos_non_attribues,
                  type(lst_data_materiel_photos_non_attribues))

            # Dans le composant "tags-selector-tagselect" on doit connaître
            # les materiel qui sont déjà sélectionnés.
            lst_data_materiel_photos_old_attribues = [item['id_materiel'] for item in
                                                            data_materiel_photos_attribues]
            session[
                'session_lst_data_materiel_photos_old_attribues'] = lst_data_materiel_photos_old_attribues
            print("lst_data_materiel_photos_old_attribues  ", lst_data_materiel_photos_old_attribues,
                  type(lst_data_materiel_photos_old_attribues))

            print(" data data_materiel_photos_selected", data_materiel_photos_selected, "type ",
                  type(data_materiel_photos_selected))
            print(" data data_materiel_photos_non_attribues ", data_materiel_photos_non_attribues, "type ",
                  type(data_materiel_photos_non_attribues))
            print(" data_materiel_photos_attribues ", data_materiel_photos_attribues, "type ",
                  type(data_materiel_photos_attribues))

            # Extrait les valeurs contenues dans la table "t_materiel", colonne "type_materiel"
            # Le composant javascript "tagify" pour afficher les tags n'a pas besoin de l'id_materiel
            lst_data_materiel_photos_non_attribues = [item['type_materiel'] for item in
                                                            data_materiel_photos_non_attribues]
            print("lst_all_materiel gf_edit_materiel_photos_selected ",
                  lst_data_materiel_photos_non_attribues,
                  type(lst_data_materiel_photos_non_attribues))

        except Exception as Exception_edit_materiel_photos_selected:
            code, msg = Exception_edit_materiel_photos_selected.args
            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Exception edit_materiel_photos_selected : {sys.exc_info()[0]} "
                  f"{Exception_edit_materiel_photos_selected.args[0]} , "
                  f"{Exception_edit_materiel_photos_selected}", "danger")

    return render_template("photos_materiel/photos_materiel_modifier_tags_dropbox.html",
                           data_materiel=data_materiel_all,
                           data_photos_selected=data_materiel_photos_selected,
                           data_materiel_attribues=data_materiel_photos_attribues,
                           data_materiel_non_attribues=data_materiel_photos_non_attribues)


"""
    nom: update_materiel_photos_selected

    Récupère la liste de tous les materiel du photos sélectionné par le bouton "MODIFIER" de "photos_materiel_afficher.html"

    Dans une liste déroulante particulière (tags-selector-tagselect), on voit :
    1) Tous les materiel contenus dans la "t_materiel".
    2) Les materiel attribués au photos selectionné.
    3) Les materiel non-attribués au photos sélectionné.

    On signale les erreurs importantes
"""


@obj_mon_application.route("/update_materiel_photos_selected", methods=['GET', 'POST'])
def update_materiel_photos_selected():
    if request.method == "POST":
        try:
            # Récupère l'id du photos sélectionné
            id_photos_selected = session['session_id_photos_materiel_edit']
            print("session['session_id_photos_materiel_edit'] ", session['session_id_photos_materiel_edit'])

            # Récupère la liste des materiel qui ne sont pas associés au photos sélectionné.
            old_lst_data_materiel_photos_non_attribues = session[
                'session_lst_data_materiel_photos_non_attribues']
            print("old_lst_data_materiel_photos_non_attribues ", old_lst_data_materiel_photos_non_attribues)

            # Récupère la liste des materiel qui sont associés au photos sélectionné.
            old_lst_data_materiel_photos_attribues = session[
                'session_lst_data_materiel_photos_old_attribues']
            print("old_lst_data_materiel_photos_old_attribues ", old_lst_data_materiel_photos_attribues)

            # Effacer toutes les variables de session.
            session.clear()

            # Récupère ce que l'utilisateur veut modifier comme materiel dans le composant "tags-selector-tagselect"
            # dans le fichier "materiel_photos_modifier_tags_dropbox.html"
            new_lst_str_materiel_photos = request.form.getlist('name_select_tags')
            print("new_lst_str_materiel_photos ", new_lst_str_materiel_photos)

            # OM 2021.05.02 Exemple : Dans "name_select_tags" il y a ['4','65','2']
            # On transforme en une liste de valeurs numériques. [4,65,2]
            new_lst_int_materiel_photos_old = list(map(int, new_lst_str_materiel_photos))
            print("new_lst_materiel_photos ", new_lst_int_materiel_photos_old,
                  "type new_lst_materiel_photos ",
                  type(new_lst_int_materiel_photos_old))

            # Pour apprécier la facilité de la vie en Python... "les ensembles en Python"
            # https://fr.wikibooks.org/wiki/Programmation_Python/Ensembles
            # OM 2021.05.02 Une liste de "id_materiel" qui doivent être effacés de la table intermédiaire "t_materiel_photos".
            lst_diff_materiel_delete_b = list(
                set(old_lst_data_materiel_photos_attribues) - set(new_lst_int_materiel_photos_old))
            print("lst_diff_materiel_delete_b ", lst_diff_materiel_delete_b)

            # Une liste de "id_materiel" qui doivent être ajoutés à la "t_materiel_photos"
            lst_diff_materiel_insert_a = list(
                set(new_lst_int_materiel_photos_old) - set(old_lst_data_materiel_photos_attribues))
            print("lst_diff_materiel_insert_a ", lst_diff_materiel_insert_a)

            # SQL pour insérer une nouvelle association entre
            # "fk_photos"/"id_photos" et "fk_materiel"/"id_materiel" dans la "t_materiel_photos"
            strsql_insert_avoir_materiel = """INSERT INTO t_avoir_materiel (id_avoir_materiel, fk_materiel, fk_photos)
                                                    VALUES (NULL, %(value_fk_materiel)s, %(value_fk_photos)s)"""

            # SQL pour effacer une (des) association(s) existantes entre "id_photos" et "id_materiel" dans la "t_materiel_photos"
            strsql_delete_materiel_photos = """DELETE FROM t_avoir_materiel WHERE fk_materiel = %(value_fk_materiel)s AND fk_photos = %(value_fk_photos)s"""

            with MaBaseDeDonnee() as mconn_bd:
                # Pour le photos sélectionné, parcourir la liste des materiel à INSÉRER dans la "t_materiel_photos".
                # Si la liste est vide, la boucle n'est pas parcourue.
                for id_materiel_ins in lst_diff_materiel_insert_a:
                    # Constitution d'un dictionnaire pour associer l'id du photos sélectionné avec un nom de variable
                    # et "id_materiel_ins" (l'id du materiel dans la liste) associé à une variable.
                    valeurs_photos_sel_materiel_sel_dictionnaire = {
                        "value_fk_photos": id_photos_selected,
                        "value_fk_materiel": id_materiel_ins}

                    mconn_bd.mabd_execute(strsql_insert_avoir_materiel,
                                          valeurs_photos_sel_materiel_sel_dictionnaire)

                # Pour le photos sélectionné, parcourir la liste des materiel à EFFACER dans la "t_materiel_photos".
                # Si la liste est vide, la boucle n'est pas parcourue.
                for id_materiel_del in lst_diff_materiel_delete_b:
                    # Constitution d'un dictionnaire pour associer l'id du photos sélectionné avec un nom de variable
                    # et "id_materiel_del" (l'id du materiel dans la liste) associé à une variable.
                    valeurs_photos_sel_materiel_sel_dictionnaire = {
                        "value_fk_photos": id_photos_selected,
                        "value_fk_materiel": id_materiel_del}

                    # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
                    # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
                    # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
                    # sera interprété, ainsi on fera automatiquement un commit
                    mconn_bd.mabd_execute(strsql_delete_materiel_photos,
                                          valeurs_photos_sel_materiel_sel_dictionnaire)

        except Exception as Exception_update_materiel_photos_selected:
            code, msg = Exception_update_materiel_photos_selected.args
            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Exception update_materiel_photos_selected : {sys.exc_info()[0]} "
                  f"{Exception_update_materiel_photos_selected.args[0]} , "
                  f"{Exception_update_materiel_photos_selected}", "danger")

    # Après cette mise à jour de la table intermédiaire "t_materiel_photos",
    # on affiche les photos et le(urs) materiel(s) associé(s).
    return redirect(url_for('photos_materiel_afficher', id_photos_sel=id_photos_selected))


"""
    nom: materiel_photos_afficher_data

    Récupère la liste de tous les materiel du photos sélectionné par le bouton "MODIFIER" de "photos_materiel_afficher.html"
    Nécessaire pour afficher tous les "TAGS" des materiel, ainsi l'utilisateur voit les materiel à disposition

    On signale les erreurs importantes
"""


def materiel_photos_afficher_data(valeur_id_photos_selected_dict):
    print("valeur_id_photos_selected_dict...", valeur_id_photos_selected_dict)
    try:

        strsql_photos_selected = """SELECT id_photos, titre_photos, lieu_photos, remarques_photos, chemin_photos, date_photos, GROUP_CONCAT(id_materiel) as Materielphotos FROM t_avoir_materiel
                                        INNER JOIN t_photos ON t_photos.id_photos = t_avoir_materiel.fk_photos
                                        INNER JOIN t_materiel ON t_materiel.id_materiel = t_avoir_materiel.fk_materiel
                                        WHERE id_photos = %(value_id_photos_selected)s"""

        strsql_materiel_photos_non_attribues = """SELECT id_materiel, type_materiel FROM t_materiel WHERE id_materiel not in(SELECT id_materiel as idMaterielphotos FROM t_avoir_materiel
                                                    INNER JOIN t_photos ON t_photos.id_photos = t_avoir_materiel.fk_photos
                                                    INNER JOIN t_materiel ON t_materiel.id_materiel = t_avoir_materiel.fk_materiel
                                                    WHERE id_photos = %(value_id_photos_selected)s)"""

        strsql_materiel_photos_attribues = """SELECT id_photos, id_materiel, type_materiel FROM t_avoir_materiel
                                            INNER JOIN t_photos ON t_photos.id_photos = t_avoir_materiel.fk_photos
                                            INNER JOIN t_materiel ON t_materiel.id_materiel = t_avoir_materiel.fk_materiel
                                            WHERE id_photos = %(value_id_photos_selected)s"""

        # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
        with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
            # Envoi de la commande MySql
            mc_afficher.execute(strsql_materiel_photos_non_attribues, valeur_id_photos_selected_dict)
            # Récupère les données de la requête.
            data_materiel_photos_non_attribues = mc_afficher.fetchall()
            # Affichage dans la console
            print("materiel_photos_afficher_data ----> data_materiel_photos_non_attribues ",
                  data_materiel_photos_non_attribues,
                  " Type : ",
                  type(data_materiel_photos_non_attribues))

            # Envoi de la commande MySql
            mc_afficher.execute(strsql_photos_selected, valeur_id_photos_selected_dict)
            # Récupère les données de la requête.
            data_photos_selected = mc_afficher.fetchall()
            # Affichage dans la console
            print("data_photos_selected  ", data_photos_selected, " Type : ",
                  type(data_photos_selected))

            # Envoi de la commande MySql
            mc_afficher.execute(strsql_materiel_photos_attribues, valeur_id_photos_selected_dict)
            # Récupère les données de la requête.
            data_materiel_photos_attribues = mc_afficher.fetchall()
            # Affichage dans la console
            print("data_materiel_photos_attribues ", data_materiel_photos_attribues, " Type : ",
                  type(data_materiel_photos_attribues))

            # Retourne les données des "SELECT"
            return data_photos_selected, data_materiel_photos_non_attribues, data_materiel_photos_attribues
    except pymysql.Error as pymysql_erreur:
        code, msg = pymysql_erreur.args
        flash(f"{error_codes.get(code, msg)} ", "danger")
        flash(f"pymysql.Error Erreur dans materiel_photos_afficher_data : {sys.exc_info()[0]} "
              f"{pymysql_erreur.args[0]} , "
              f"{pymysql_erreur}", "danger")
    except Exception as exception_erreur:
        code, msg = exception_erreur.args
        flash(f"{error_codes.get(code, msg)} ", "danger")
        flash(f"Exception Erreur dans materiel_photos_afficher_data : {sys.exc_info()[0]} "
              f"{exception_erreur.args[0]} , "
              f"{exception_erreur}", "danger")
    except pymysql.err.IntegrityError as IntegrityError_materiel_photos_afficher_data:
        code, msg = IntegrityError_materiel_photos_afficher_data.args
        flash(f"{error_codes.get(code, msg)} ", "danger")
        flash(f"pymysql.err.IntegrityError Erreur dans materiel_photos_afficher_data : {sys.exc_info()[0]} "
              f"{IntegrityError_materiel_photos_afficher_data.args[0]} , "
              f"{IntegrityError_materiel_photos_afficher_data}", "danger")


"""
    Auteur : OM 2021.03.22
    Définition d'une "route" /photos_materiel_ajouter

    Test : ex : http://127.0.0.1:5005/photos_ajouter

    Paramètres : sans

    But : Ajouter un photos pour un film

    Remarque :  Dans le champ "name_photos_html" du formulaire "photos/photos_ajouter.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/photos_materiel_ajouter", methods=['GET', 'POST'])
def photos_materiel_ajouter_wtf():
    form = FormWTFAjouterphotosMateriel()
    if request.method == "POST":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion photos ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur Gestion photos {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            if form.validate_on_submit():
                titre_photos_wtf = form.titre_photos_wtf.data
                titre_photos = titre_photos_wtf.capitalize()

                lieu_photos_wtf = form.lieu_photos_wtf.data
                lieu_photos = lieu_photos_wtf.capitalize()

                remarques_photos_wtf = form.remarques_photos_wtf.data
                remarques_photos = remarques_photos_wtf.capitalize()

                date_photos_wtf = form.date_photos_wtf.data
                date_photos = date_photos_wtf.capitalize()



                valeurs_insertion_dictionnaire = {"value_titre_photos": titre_photos,
                                                  "value_lieu_photos": lieu_photos,
                                                  "value_remarques_photos": remarques_photos,
                                                  "value_date_photos": date_photos}

                print("valeurs_insertion_dictionnaire ", valeurs_insertion_dictionnaire)

                strsql_insert_photos = """INSERT INTO t_photos (id_photos,titre_photos,lieu_photos,remarques_photos,date_photos) VALUES (NULL,%(value_titre_photos)s,%(value_lieu_photos)s,%(value_remarques_photos)s,%(value_date_photos)s)"""
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(strsql_insert_photos, valeurs_insertion_dictionnaire)

                flash(f"Données insérées !!", "success")
                print(f"Données insérées !!")

                # Pour afficher et constater l'insertion de la valeur, on affiche en ordre inverse. (DESC)
                return redirect(url_for('photos_materiel_afficher', order_by='DESC', id_photos_sel=0))

        # ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur_photos_doublon:
            # Dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs/exceptions.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            code, msg = erreur_photos_doublon.args

            flash(f"{error_codes.get(code, msg)} ", "warning")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur_gest_matos_crud:
            code, msg = erreur_gest_matos_crud.args

            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Erreur dans Gestion photos CRUD : {sys.exc_info()[0]} "
                  f"{erreur_gest_matos_crud.args[0]} , "
                  f"{erreur_gest_matos_crud}", "danger")

    return render_template("photos_materiel/photos_materiel_ajouter_wtf.html", form=form)


"""
    Auteur : OM 2021.03.29
    Définition d'une "route" /photos_update

    Test : ex cliquer sur le menu "photos" puis cliquer sur le bouton "EDIT" d'un "photos"

    Paramètres : sans

    But : Editer(update) un photos qui a été sélectionné dans le formulaire "photos_afficher.html"

    Remarque :  Dans le champ "titre_photos_update_wtf" du formulaire "photos/photos_update_wtf.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/photos_materiel_update", methods=['GET', 'POST'])
def photos_materiel_update_wtf():
    # L'utilisateur vient de cliquer sur le bouton "EDIT". Récupère la valeur de "id_photos"
    id_photos_update = request.values['id_photos_btn_edit_html']

    # Objet formulaire pour l'UPDATE
    form_update = FormWTFUpdatephotosMateriel()
    try:
        print(" on submit ", form_update.validate_on_submit())
        if form_update.validate_on_submit():
            # Récupèrer la valeur du champ depuis "photos_update_wtf.html" après avoir cliqué sur "SUBMIT".
            # Puis la convertir en lettres minuscules.
            name_photos_update = form_update.titre_photos_update_wtf.data
            name_photos_update = name_photos_update

            valeur_update_dictionnaire = {"value_id_photos": id_photos_update,
                                          "value_name_photos": name_photos_update}
            print("valeur_update_dictionnaire ", valeur_update_dictionnaire)

            str_sql_update_titre_photos = """UPDATE t_photos SET titre_photos = %(value_name_photos)s WHERE id_photos = %(value_id_photos)s"""
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(str_sql_update_titre_photos, valeur_update_dictionnaire)

            flash(f"Donnée mise à jour !!", "success")
            print(f"Donnée mise à jour !!")

            # afficher et constater que la donnée est mise à jour.
            # Affiche seulement la valeur modifiée, "ASC" et l'"id_photos_update"
            return redirect(
                url_for('photos_afficher', order_by="ASC", id_photos_sel=id_photos_update))
        elif request.method == "GET":
            # Opération sur la BD pour récupérer "id_photos" et "noms" de la "t_photos"
            str_sql_id_photos = "SELECT id_photos, titre_photos FROM t_photos WHERE id_photos = %(value_id_photos)s"
            valeur_select_dictionnaire = {"value_id_photos": id_photos_update}
            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()
            mybd_curseur.execute(str_sql_id_photos, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()", vu qu'il n'y a qu'un seul champ "nom photos" pour l'UPDATE
            data_nom_emplacement = mybd_curseur.fetchone()
            print("data_nom_emplacement ", data_nom_emplacement, " type ", type(data_nom_emplacement), " photos ",
                  data_nom_emplacement["titre_photos"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "photos_update_wtf.html"
            form_update.titre_photos_update_wtf.data = data_nom_emplacement["titre_photos"]

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans photos_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans photos_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_matos_crud:
        code, msg = erreur_gest_matos_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_matos_crud} ", "danger")
        flash(f"Erreur dans photos_update_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_matos_crud.args[0]} , "
              f"{erreur_gest_matos_crud}", "danger")
        flash(f"__KeyError dans photos_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("photos/photos_update_wtf.html", form_update=form_update)


"""
    Auteur : OM 2021.04.08
    Définition d'une "route" /photos_delete

    Test : ex. cliquer sur le menu "photos" puis cliquer sur le bouton "DELETE" d'un "photos"

    Paramètres : sans

    But : Effacer(delete) un photos qui a été sélectionné dans le formulaire "photos_afficher.html"

    Remarque :  Dans le champ "titre_photos_delete_wtf" du formulaire "photos/photos_delete_wtf.html",
                le contrôle de la saisie est désactivée. On doit simplement cliquer sur "DELETE"
"""


@obj_mon_application.route("/photos_materiel_delete", methods=['GET', 'POST'])
def photos_materiel_delete_wtf():
    data_photos_attribue_photos_delete = None
    btn_submit_del = None
    # L'utilisateur vient de cliquer sur le bouton "DELETE". Récupère la valeur de "id_photos"
    id_photos_delete = request.values['id_photos_btn_delete_html']

    # Objet formulaire pour effacer le photos sélectionné.
    form_delete = FormWTFDeletephotosMateriel()
    try:
        print(" on submit ", form_delete.validate_on_submit())
        if request.method == "POST" and form_delete.validate_on_submit():

            if form_delete.submit_btn_annuler.data:
                return redirect(url_for("photos_afficher", order_by="ASC", id_photos_sel=0))

            if form_delete.submit_btn_conf_del.data:
                # Récupère les données afin d'afficher à nouveau
                # le formulaire "materiel/materiel_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
                data_materiel_attribue_photos_delete = session['data_materiel_attribue_photos_delete']
                print("data_materiel_attribue_photos_delete ", data_materiel_attribue_photos_delete)

                flash(f"Etes-vous sur de vouloir l'effacer de la DB ? C'est définitif.", "danger")
                # L'utilisateur vient de cliquer sur le bouton de confirmation pour effacer...
                # On affiche le bouton "Effacer materiel" qui va irrémédiablement EFFACER le materiel
                btn_submit_del = True

            if form_delete.submit_btn_del.data:
                valeur_delete_dictionnaire = {"value_id_photos": id_photos_delete}
                print("valeur_delete_dictionnaire ", valeur_delete_dictionnaire)

                str_sql_delete_materiel_photos = """DELETE FROM t_avoir_materiel WHERE fk_photos = %(value_id_photos)s"""
                str_sql_delete_id_photos = """DELETE FROM t_photos WHERE id_photos = %(value_id_photos)s"""
                # Manière brutale d'effacer d'abord la "fk_materiel", même si elle n'existe pas dans la "t_materiel_film"
                # Ensuite on peut effacer le materiel vu qu'il n'est plus "lié" (INNODB) dans la "t_materiel_film"
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(str_sql_delete_materiel_photos, valeur_delete_dictionnaire)
                    mconn_bd.mabd_execute(str_sql_delete_id_photos, valeur_delete_dictionnaire)

                flash(f"materiel définitivement effacé !!", "success")
                print(f"materiel définitivement effacé !!")

                # afficher les données
                return redirect(url_for('photos_afficher', order_by="ASC", id_photos_sel=0))

        if request.method == "GET":
            valeur_select_dictionnaire = {"value_id_photos": id_photos_delete}
            print(id_photos_delete, type(id_photos_delete))

            # Requête qui affiche tous les films qui ont le materiel que l'utilisateur veut effacer
            str_sql_materiel_photos_delete = """SELECT id_avoir_materiel, type_materiel, id_photos, titre_photos FROM t_avoir_materiel
                                            INNER JOIN t_materiel ON t_avoir_materiel.fk_materiel = t_materiel.id_materiel
                                            INNER JOIN t_photos ON t_avoir_materiel.fk_photos = t_photos.id_photos
                                            WHERE fk_photos = %(value_id_photos)s"""

            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()

            mybd_curseur.execute(str_sql_materiel_photos_delete, valeur_select_dictionnaire)
            data_materiel_attribue_photos_delete = mybd_curseur.fetchall()
            print("data_materiel_attribue_photos_delete...", data_materiel_attribue_photos_delete)

            # Nécessaire pour mémoriser les données afin d'afficher à nouveau
            # le formulaire "materiel/materiel_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
            session['data_materiel_attribue_photos_delete'] = data_materiel_attribue_photos_delete

            # Opération sur la BD pour récupérer "id_materiel" et "type_materiel" de la "t_materiel"
            str_sql_id_photos = "SELECT id_photos, titre_photos FROM t_photos WHERE id_photos = %(value_id_photos)s"

            mybd_curseur.execute(str_sql_id_photos, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()",
            # vu qu'il n'y a qu'un seul champ "nom materiel" pour l'action DELETE
            data_titre_photos = mybd_curseur.fetchone()
            print("data_titre_photos ", data_titre_photos, " type ", type(data_titre_photos),
                  " photos ",
                  data_titre_photos["titre_photos"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "materiel_delete_wtf.html"
            form_delete.titre_photos_delete_wtf.data = data_titre_photos["titre_photos"]

            # Le bouton pour l'action "DELETE" dans le form. "materiel_delete_wtf.html" est caché.
            btn_submit_del = False

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans photos_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans photos_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_matos_crud:
        code, msg = erreur_gest_matos_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_matos_crud} ", "danger")

        flash(f"Erreur dans photos_delete_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_matos_crud.args[0]} , "
              f"{erreur_gest_matos_crud}", "danger")

        flash(f"__KeyError dans photos_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("photos_materiel/photos_materiel_delete_wtf.html",
                           form_delete=form_delete,
                           btn_submit_del=btn_submit_del,
                           data_photos_associes=data_materiel_attribue_photos_delete)
