-- Détection si une autre base de donnée du même nom existe

DROP DATABASE IF EXISTS vanderaa_jim_info1a_photo_104;

-- Création d'un nouvelle base de donnée

CREATE DATABASE IF NOT EXISTS vanderaa_jim_info1a_photo_104;

-- Utilisation de cette base de donnée

USE vanderaa_jim_info1a_photo_104;

-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Dim 13 Juin 2021 à 17:28
-- Version du serveur :  5.6.20-log
-- Version de PHP :  5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `vanderaa_jim_info1a_photo_104`
--

-- --------------------------------------------------------

--
-- Structure de la table `t_avoir_materiel`
--

CREATE TABLE IF NOT EXISTS `t_avoir_materiel` (
`id_avoir_materiel` int(11) NOT NULL,
  `fk_photos` int(11) NOT NULL,
  `fk_materiel` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Contenu de la table `t_avoir_materiel`
--

INSERT INTO `t_avoir_materiel` (`id_avoir_materiel`, `fk_photos`, `fk_materiel`) VALUES
(1, 1, 1),
(3, 1, 3),
(7, 2, 1),
(8, 2, 3),
(9, 17, 4),
(10, 17, 13),
(11, 19, 13);

-- --------------------------------------------------------

--
-- Structure de la table `t_materiel`
--

CREATE TABLE IF NOT EXISTS `t_materiel` (
`id_materiel` int(11) NOT NULL,
  `type_materiel` varchar(50) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Contenu de la table `t_materiel`
--

INSERT INTO `t_materiel` (`id_materiel`, `type_materiel`) VALUES
(1, 'Sigma 50mm 1.8f'),
(2, 'Sigma 85mm 1.4f'),
(3, 'Sony A7iii'),
(4, 'Canon EOS 750D'),
(9, 'DJI Mavic Pro'),
(10, 'DJI Mavic Mini'),
(13, 'Blackmagic 6k');

-- --------------------------------------------------------

--
-- Structure de la table `t_personnes`
--

CREATE TABLE IF NOT EXISTS `t_personnes` (
`id_personnes` int(11) NOT NULL,
  `nom_personnes` varchar(42) DEFAULT NULL,
  `prenom_personnes` varchar(42) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Contenu de la table `t_personnes`
--

INSERT INTO `t_personnes` (`id_personnes`, `nom_personnes`, `prenom_personnes`) VALUES
(20, 'Van der aa', 'Jim'),
(21, 'Soan', 'Robin'),
(22, 'Lazare', 'Patrick'),
(23, 'Alfonso', 'Achille'),
(24, 'Turpin', 'Léonard');

-- --------------------------------------------------------

--
-- Structure de la table `t_photos`
--

CREATE TABLE IF NOT EXISTS `t_photos` (
`id_photos` int(11) NOT NULL,
  `titre_photos` longtext,
  `lieu_photos` longtext,
  `remarques_photos` longtext,
  `chemin_photos` longtext,
  `date_photos` date DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Contenu de la table `t_photos`
--

INSERT INTO `t_photos` (`id_photos`, `titre_photos`, `lieu_photos`, `remarques_photos`, `chemin_photos`, `date_photos`) VALUES
(1, 'Ford Must', 'Meyrin, GE', 'Stage 3 Mustang, Plaques Dubai.', '\\static\\images\\photos\\mustang.jpg', '2021-05-18'),
(2, 'Audi TTS', 'Saint-George, VD', 'Audi TTS, Stage 2, AirLift', '\\static\\images\\photos\\tts.jpg', '2021-05-14'),
(17, 'Audi R8', 'Aubonne, VD', 'R8 V10 Plus,\r\n650 Chevaux', '\\static\\images\\photos\\R8.jpg', '2021-05-27'),
(19, 'Audi RS3', 'Satigny, GE', 'Audi RS3, Préparée avec plus de 500 chevaux.', '\\static\\images\\photos\\rs3.jpg', '2020-11-17'),
(20, 'BMW M2 Competition', 'Gland, VD', 'M2 préparée, ethanol, avec full-tube Capristo', '\\static\\images\\photos\\m2.jpg', '2021-05-29');

-- --------------------------------------------------------

--
-- Structure de la table `t_photos_personnes`
--

CREATE TABLE IF NOT EXISTS `t_photos_personnes` (
`id_photos_personnes` int(11) NOT NULL,
  `fk_photos` int(11) NOT NULL,
  `fk_personnes` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Contenu de la table `t_photos_personnes`
--

INSERT INTO `t_photos_personnes` (`id_photos_personnes`, `fk_photos`, `fk_personnes`) VALUES
(14, 1, 20),
(15, 2, 24),
(16, 17, 23),
(17, 19, 20),
(18, 20, 21);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `t_avoir_materiel`
--
ALTER TABLE `t_avoir_materiel`
 ADD PRIMARY KEY (`id_avoir_materiel`), ADD KEY `fk_photos` (`fk_photos`), ADD KEY `fk_materiel` (`fk_materiel`);

--
-- Index pour la table `t_materiel`
--
ALTER TABLE `t_materiel`
 ADD PRIMARY KEY (`id_materiel`);

--
-- Index pour la table `t_personnes`
--
ALTER TABLE `t_personnes`
 ADD PRIMARY KEY (`id_personnes`);

--
-- Index pour la table `t_photos`
--
ALTER TABLE `t_photos`
 ADD PRIMARY KEY (`id_photos`);

--
-- Index pour la table `t_photos_personnes`
--
ALTER TABLE `t_photos_personnes`
 ADD PRIMARY KEY (`id_photos_personnes`), ADD KEY `fk_photos` (`fk_photos`), ADD KEY `fk_materiel` (`fk_personnes`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `t_avoir_materiel`
--
ALTER TABLE `t_avoir_materiel`
MODIFY `id_avoir_materiel` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT pour la table `t_materiel`
--
ALTER TABLE `t_materiel`
MODIFY `id_materiel` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT pour la table `t_personnes`
--
ALTER TABLE `t_personnes`
MODIFY `id_personnes` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT pour la table `t_photos`
--
ALTER TABLE `t_photos`
MODIFY `id_photos` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT pour la table `t_photos_personnes`
--
ALTER TABLE `t_photos_personnes`
MODIFY `id_photos_personnes` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `t_avoir_materiel`
--
ALTER TABLE `t_avoir_materiel`
ADD CONSTRAINT `t_avoir_materiel_ibfk_1` FOREIGN KEY (`fk_photos`) REFERENCES `t_photos` (`id_photos`),
ADD CONSTRAINT `t_avoir_materiel_ibfk_2` FOREIGN KEY (`fk_materiel`) REFERENCES `t_materiel` (`id_materiel`);

--
-- Contraintes pour la table `t_photos_personnes`
--
ALTER TABLE `t_photos_personnes`
ADD CONSTRAINT `t_photos_personnes_ibfk_1` FOREIGN KEY (`fk_photos`) REFERENCES `t_photos` (`id_photos`),
ADD CONSTRAINT `t_photos_personnes_ibfk_2` FOREIGN KEY (`fk_personnes`) REFERENCES `t_personnes` (`id_personnes`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
