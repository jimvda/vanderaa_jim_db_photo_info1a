Photo DB - Informations Utiles
Jim van der Aa - INFO 1A
---
* Vous devez installer les logiciels suivants pour pouvoir lancer mon projet. 
UWAMP (https://www.uwamp.com/file/UwAmp.exe) si problèmes (https://www.uwamp.com/fr/?page=download)

* Si impossible à faire fonctionner, télécharger le zip et décompresser-le dans un répertoire. Puis double-clic sur UwAmp.exe (https://www.uwamp.com/file/UwAmp.zip)

* Python : https://www.python.org/downloads/
* Pycharm : https://www.jetbrains.com/fr-fr/student/

* Si l'installation est réussie, on peut procéder au lancement.

* Pour accéder à mon projet il suffit de télécharger le projet depuis mon GitLab, ou l'ouvrir directement sur PyCharm si vous l'avez sur la machine en local.
Mon Lien GitLab : https://gitlab.com/jimvda/vanderaa_jim_db_photo_info1a

* Lancez Pycharm. Cliquez sur GET FROM CVS, saisisez ensuite mon lien GitLab, et il faut ensuite définir un endroit pour le sauuvgarder sur votre machine.
* Lorsque cela est fait, cliquez sur "Clone" et Pycharm téléchargera le projet depuis GitLab.

* Il faudra ensuite installer l'interpreter Python, cliquez sur "Oui", lorsque le Pop-Up apparaît.

* Ensuite, demarrer UwAmp, pourr lancer le server MySql.
* Ensuite, importer la base de données grâce à un "run" du fichier "zzzdemos/1_ImportationDumpSql.py" sur PyCharm.
* Pour lancer le site, faites un "run" sur le fichier "1_run_server_flask.py" qui se trouve à la racine du projet.
* Le projet chargera ensuite, et dans le terminal, un lien apparaîtra avec l'URL du site.

* URL DU SITE : http://127.0.0.1:5005/

* Maintenant, la base de données est prête, vous pouvez vous amuser à modifier les tables, ajouter des photos, des utilisateurs, etc..
---
Jim van der Aa :-)
















